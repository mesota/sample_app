class Micropost < ApplicationRecord
  belongs_to  :user
  belongs_to  :in_reply_to, class_name: "User", 
                            foreign_key: "in_reply_to_id", 
                            optional: true
  composed_of :content_object, class_name: "MicropostContent",
                               mapping: %w(content micropost_content)
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  before_validation :assign_in_reply_to
  validates      :user_id, presence: true
  validates      :content, length: { maximum: 140 }
  validates      :content_object, presence: true
  validates_with ReplyValidator, if: -> { content_object.reply? }
  validate       :picture_size
  
  def reply?
    !in_reply_to.nil?
  end
  
  private
    
    def assign_in_reply_to
      if content_object.reply?
        self.in_reply_to = User.find_by(id: content_object.reply_name.user_id)
      end
    end
    
    def self.including_replies(user_id)
      where("user_id = :user_id OR in_reply_to_id = :user_id", user_id: user_id)
    end
    
    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
