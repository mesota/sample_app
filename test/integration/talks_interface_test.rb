require 'test_helper'

class TalksInterfaceTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:user_1)
  end
  
  test "talks interface" do
    log_in_as(@user)
    get root_path
    # トークタブ
    assert_select 'div#talks-tab ul.pagination'
    # メンバーシップを削除する
    assert_select 'a', text: 'leave'
    first_talk = @user.talks.first
    assert_difference 'Membership.count', -1 do
      first_talk.leave(@user)
    end
    assert_not @user.reload.talks.include?(first_talk)
    # 違うユーザーのプロフィールにアクセス (talksタブがないことを確認)
    get user_path(users(:michael))
    assert_select 'div#talks-tab ul.pagination', count: 0
  end
end
