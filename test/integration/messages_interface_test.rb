require 'test_helper'

class MessagesInterfaceTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:user_1)
    @talk = @user.talks.first
  end
  
  test "messages_interface" do
    log_in_as(@user)
    get talk_path(@talk)
    # 無効なメッセージ送信
    assert_no_difference 'Message.count' do
      post messages_talk_path(@talk), params: { message: { user: nil, content: "" } }
    end
    assert_select 'div#error_explanation'
    # 有効なメッセージ送信
    content = "test message"
    assert_difference 'Message.count', 1 do
      post messages_talk_path(@talk), params: { message: { user: @user,
                                                           content: content } }
    end
    assert_redirected_to @talk
    follow_redirect!
    assert_match content, response.body
    # メッセージを削除する
    assert_select 'a', text: 'delete'
    first_message = @talk.messages.where(user: @user).first
    assert_difference 'Message.count', -1 do
      delete message_path(first_message, talk_id: @talk)
    end
  end
end
