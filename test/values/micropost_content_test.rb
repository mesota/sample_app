require 'test_helper'

class MicropostContentTest < ActiveSupport::TestCase
  
  def setup
    reply_name = ReplyName.new(1, "Test User")
    content = Faker::Lorem.sentence(5)
    @micropost_content = MicropostContent.new(content)
    @reply_content     = MicropostContent.new(reply_name.to_s + " " + content)
  end
  
  test "reply name should be valid when reply" do
    assert_not @micropost_content.reply_name.valid?
    assert @reply_content.reply_name.valid?
  end
  
  test "content except reply name when reply" do
    assert_equal @micropost_content.content, @reply_content.content
  end
  
  test "reply content is reply" do
    assert_not @micropost_content.reply?
    assert @reply_content.reply?
  end
end
