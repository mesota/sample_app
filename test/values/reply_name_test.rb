require 'test_helper'

class ReplyNameTest < ActiveSupport::TestCase
  
  def setup
    @reply_name = ReplyName.new(1, "Test User")
  end
  
  test "valid reply name should be present user id and user name" do
    assert @reply_name.valid?
  end
  
  test "to string returns '@{user id}-{user-name}'" do
    assert_equal "@1-Test-User", @reply_name.to_s
  end
end
