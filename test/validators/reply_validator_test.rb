require 'test_helper'

class ReplyValidatorTest < ActiveSupport::TestCase
  
  def setup
    @dummy_model = Struct.new(:in_reply_to, :content_object, :user) do
      include ActiveModel::Validations
      
      def self.name
        'DummyModel'
      end
      
      validates_with ReplyValidator
    end
    @michael = users(:michael)
  end
  
  test "in reply to should be present" do
    in_reply_to = nil
    assert_not @dummy_model.new(in_reply_to).valid?
  end
  
  test "in reply to should be activated" do
    in_reply_to = @michael
    in_reply_to.activated = false
    assert_not @dummy_model.new(in_reply_to).valid?
  end
  
  test "in reply to should be equal to the reply name of content" do
    in_reply_to = @michael
    content_object = MicropostContent.new("#{users(:archer).reply_name} content")
    assert_not @dummy_model.new(in_reply_to, content_object).valid?
  end
  
  test "in reply to should be different from user" do
    in_reply_to = @michael
    content_object = MicropostContent.new("#{@michael.reply_name} content")
    user = @michael
    assert_not @dummy_model.new(in_reply_to, content_object, user).valid?
  end
end
