require 'test_helper'

class MembershipsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @talk = talks(:talk_1)
    @user = users(:michael)
    @membership = Membership.create(talk: @talk, user: @user)
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference "Membership.count" do
      delete membership_path(@membership, talk_id: @talk)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy for wrong membership" do
    log_in_as(users(:archer))
    assert_no_difference "Membership.count" do
      delete membership_path(@membership, talk_id: @talk)
    end
    assert_redirected_to root_url
  end
end
