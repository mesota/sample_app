require 'test_helper'

class TalksControllerTest < ActionDispatch::IntegrationTest
  
  test "should redirect show when not logged in" do
    get talk_path(talks(:talk_1))
    assert_redirected_to login_url
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference "Talk.count" do
      post talks_path, params: { member_ids: [users(:michael).id] }
    end
    assert_redirected_to login_url
  end
end
