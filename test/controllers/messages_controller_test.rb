require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @talk = talks(:talk_1)
    @user = @talk.members.first
    @wrong_user = users(:michael)
    @message = Message.create(talk: @talk, 
                              user: @user, 
                              content: Faker::Lorem.sentence(5))
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference "Message.count" do
      post messages_talk_path(@talk), params: { user: @user,
                                      content: Faker::Lorem.sentence(5) }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect create for wrong member" do
    log_in_as(@wrong_user)
    assert_no_difference "Message.count" do
      post messages_talk_path(@talk), params: { user: @wrong_user,
                                      content: Faker::Lorem.sentence(5) }
    end
    assert_redirected_to root_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference "Message.count" do
      delete message_path(@message, talk_id: @talk)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy for wrong user" do
    log_in_as(@wrong_user)
    assert_no_difference "Message.count" do
      delete message_path(@message, talk_id: @talk)
    end
    assert_redirected_to root_url
  end
end
