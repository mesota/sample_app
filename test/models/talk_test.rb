require 'test_helper'

class TalkTest < ActiveSupport::TestCase
  
  def setup
    @talk = Talk.create
    10.times do |t|
      user = users("user_#{t}".to_sym)
      @talk.memberships.create(user: user)
      @talk.messages.create(user: user, content: Faker::Lorem.sentence(5))
    end
    @user = users(:user_0)
  end
  
  test "too long title should be omitted" do
    title = "User 1, User 2, User 3, User 4, User 5, User ... (10)"
    assert_equal @talk.title(@user, 50), title
  end
  
  test "title should not be include current user" do
    title = @talk.title(@user, 50)
    assert_not title.include?(@user.name)
  end
  
  test "associated memberships should be destroyed" do
    assert_difference "Membership.count", -10 do
      @talk.destroy
    end
  end
  
  test "associated messages should be destroyed" do
    assert_difference "Message.count", -10 do
      @talk.destroy
    end
  end
end
