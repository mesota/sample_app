# ユーザー
User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |t|
  name = Faker::Name.name
  email = "example-#{t+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

# マイクロポスト
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# リレーションシップ
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

# リプライ
to_user    = users.first
from_users = users[2..50]
from_users.each do |from_user|
  content = to_user.reply_name.to_s + " " + Faker::Lorem.sentence(5)
  from_user.microposts.create!(content: content)
end

# トーク
user = users.first
members = users[2..30]
members.each do |member|
  talk = Talk.create!
  # メンバーシップ
  talk.memberships.create!(user: user)
  talk.memberships.create!(user: member)
  # メッセージ
  talk.messages.create!(user: user, content: Faker::Lorem.sentence(5))
  talk.messages.create!(user: member, content: Faker::Lorem.sentence(5))
end
